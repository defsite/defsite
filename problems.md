
# Problems with `defsite`

(This file is probably better suited in the defsite repo, though at the end there is a point regarding this site)

I have encountered some problems when setting up / implementing defsite, that I'm not sure if I can resolve them.

1. No static typing. I found this to be more and more of a problem as I coded along.
   I already had this problem with Clojure before (with the `books-app`) and I'm getting there again.
   I could use the REPL, but that isn't perfect even when it works.
   I personally simply really like to be able to press CTRL+Q.
   Also regarding that, see (2).
2. Cannot really work with the REPL, because the files aren't written out to the output folder.
   But maybe I can arrange this, then that would make the whole criticism here a lot less.
   I need to change the `defpage`/`deftemplate` functions, however. See GH issue #7.

I think that this is more work than for example using Rust as backend + Svelte (with mdsvex) as frontend.

But maybe I'm wrong? Maybe, once `defsite` is finished (*and* if (2) turns out to be wrong, i.e. REPL development works),
it all actually works pretty fine and fast (regarding development speed)?

I still have the problem of dynamic typing, which is a *personal* problem, but I still don't like it so much anymore.

Another problem is how to organize the MD files. Obsidian would be nice here.


#### Idea

I did have the idea of switching to Rust at the backend and Svelte+mdsvex at the frontend.

Rust has `obsidian_export`, which would be nice, because then I can create the EU Knowledge base in Obsidian.

Thus, I'd have Obsidian as a frontend for writing stuff.

However, regarding Obsidian, that would most likely also be able to get to work with `defsite`


#### Other Problem

I think I might also just want to use Rust right now (and Svelte) instead of trying to finish this project.

I have actually just finished v0.2.0 for defsite, and I should definitely try it out further.
Especially, since the REPL currently doesn't work, I should focus on that (see GH issue#7).
