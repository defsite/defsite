
# Notes on Clojure Spec

We don't use it extensively, but try to use it wherever it helps.

We can use `s/valid?` in `:pre` and `:post` where it makes sense - look at the examples in the code.

We also use `s/conform` to destructure them. Regarding destructuring:

We actually don't really want to use keywords, so we always specify `:req-un`/`:opt-un` (un = unqualified, thus we don't
need to use qualified keywords). Regarding that, also see this article:
https://vvvvalvalval.github.io/posts/clojure-key-namespacing-convention-considered-harmful.html#'but_clojure.spec_encourages_the_use_of_clojure-namespaced_keywords!'

## Regarding s/valid? and s/conform

We should "overwrite" / extend them with our own implementation, which basically calls these, but if they're invalid,
aborts and gives a nice error back!
