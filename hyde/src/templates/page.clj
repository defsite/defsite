(ns templates.page
  (:require
    [deftemplate :refer [deftemplate-page]]))

(deftemplate-page
  {:template-fn
   (fn [site page]
     [:html {:lang "en"}
      [:head
       [:meta {:http-equiv "X-UA-Compatible" :content "IE=edge"}]
       [:meta {:http-equiv "content-type" :content "text/html; charset=utf-8"}]
       [:meta {:name "viewport" :content "width=device-width, initial-scale=1.0, maximum-scale=1, viewport-fit=cover"}]
       [:title {:title site}]
       [:link {:rel "stylesheet" :href (str (-> site :paths :static-content) "print.css") :media "print"}]
       [:link {:rel "stylesheet" :href (str (-> site :paths :static-content) "poole.css")}]
       [:link {:rel "stylesheet" :href (str (-> site :paths :static-content) "hyde.css")}]
       [:link {:rel "stylesheet" :href "https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700|Abril+Fatface"}]

       ;; TODO: Maybe later we want to be able to create a feed
       ;"{% if config.generate_feed %}"
       ;[:link {:rel "alternate" :type "{%" :if "true" :config.feed_filename "="}]
       ;"\"atom.xml\" %}\"application/atom+xml\"{% else %}\"application/rss+xml\"{% endif %} title=\"RSS\" href=\"{{ get_url(path=config.feed_filename) | safe }}\">
       ;{% endif %}"
       ]

      [:body
       [:div.sidebar
        [:div.container.sidebar-sticky
         [:div.sidebar-about
          [:a {:href (:base-url site)} [:h1 (:title site)]]
          (if (:description site)
            [:p.lead (:description site)])]

         [:ul.sidebar-nav
          (for [link (:hyde-links (:extra site))]
            [:li.sidebar-nav-item [:a {:href (:url link)} (:name link)]])]]]

       [:div.content.container
        [:div.post
         [:h1.post-title (:title page)]
         [:span.post-date "{{ page.date | date(format=\"%Y-%m-%d\") }}"]
         ;; TODO: Print the page content!!! <<<<<<< DO this
         ]]]])})
