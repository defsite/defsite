(ns site
  (:require
    [defsite]))

(defn -main
  []
  (defsite/defsite
    ;; TODO: change base-url
    {:base-url    "https://zola-hyde.netlify.com"
     :title       "Hyde Theme for defsite"
     :description "A clean blogging theme"
     :extra
     {:hyde-links [{:url "https://google.com" :name "About"}]}}))
