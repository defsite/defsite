(ns content.some-other-article
  (:require
    [defpage :refer [defpage]]))

(defpage
  {:title "A first theme for defsite"
   :md-file "some-other-article.md"})
