
# Usage

## Add Dependency

First, we add the dependency; here, we use `deps.edn` and a local dependency:

```clojure
{:deps {defsite/defsite {:local/root ".."}}}
```

This will get the newest source. To get a specific version (that has been installed locally), add this instead:

```clojure
{:deps {defsite/defsite {:mvn/version "0.2.0"}}}
```

To check that it works, run `clj -P`. If nothing is printed, it works!


### Add `site` to `deps.edn`

We might want to put site-specific stuff (like content, templates, etc.) into a `site` folder, and code into a `src`
folder.

To do that, define these paths in `deps.edn`. Note that this is optional, but it helps to separate the site from the
code.

```clojure
{:paths ["src" "site"]
 :dependencies ...}
```


## Define the Site

In the `site` root, create `site.clj` with this content:

```clojure
(ns site
  (:require
    [defsite :refer [defsite]]))

(defn -main
  []
  (defsite
    {:base-url    "https://zola-hyde.netlify.com"
     :title       "Hyde Theme for defsite"
     :description "A clean blogging theme"
     :extra
     {:hyde-links [{:url "https://google.com" :name "About"}))
```

We now have the site defined! Next, we will add some content.

## Add Templates

To generate the actual pages in the end, we (1) take the templates and (2) if needed, add the content to them.
Whether we add content solely depends on the template, i.e. whether this template displays content or not.

The most important template is the `index` template, i.e. the homepage template.
Create a `templates` package and in there create an `index.clj`:

```clojure
(ns templates.index
  (:require
    [deftemplate :refer [deftemplate-index]]))

(deftemplate-index
  {:name :index
   :template-fn
   (fn [site]
     [:html {:lang "en"}
      [:head
       ;; ...
       ]
      [:body
       ;; ...
       ]])})
```

Have a look the example Hyde site for how to fill it.

Regarding the given parameter `side`: This is the full site at the point of generation, which will include all
other templates, as well as all pages.

## Add Content

[//]: # (TODO: Later, I also want to be able to define them directly as MD files - w/o any CLJ file!)
Content, or pages, can be defined with `(defpage ...)`. Where this is called generally doesn't matter. The only 
requirement is that it's inside (below) the `content` namespace.

We will define an example article. We put it in the file `site/content/some_article.clj`:

```clojure
;; TODO
```

## Generate the site

Now we have everything so generate the site.

To actually generate it now, run the following:

```shell
clj -Mrun
```