(ns defsite.theme
  (:require
    [clojure.string :as str]))

;; TODO: We should use something like (deftheme ...) instead
;;  and we'd kind of need some way that we can define a standard theme, and then be able to override that
;;  we could however actually simply start by doing all theme definitions (just as template definitions) on the user
;;  side
(defn add-tailscale-classes-for-md-content [md state]
  ;; TODO: Change other classes as well - create a nicer design using tailscale
  (if (:inline-heading state)
    [(clojure.string/replace md "<h1>" "<h1 class=\"text-4xl mb-2\">")]
    [md state]))

(defn tailwind
  [& tags]
  (letfn [(convert-tag [tag]
            (if (keyword? tag)
              (if (empty? (namespace tag))
                (name tag)
                (str (namespace tag) ":" (name tag)))
              tag))]
    (str/join " " (map convert-tag tags))))

