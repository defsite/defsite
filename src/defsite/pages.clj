(ns defsite.pages
  (:require
    [clojure.string :as str]
    [defsite.site :refer [site]])
  (:import (java.net URLEncoder)))

(defn- add-output-file-name
  [page]
  (if (contains? page :url-slug)
    (assoc page :output-filename (:url-slug page))
    (assoc page :output-filename (-> page :title (str/lower-case) (URLEncoder/encode "UTF-8")))))

(comment
  (add-output-file-name {:title "Hello There!"})
  (add-output-file-name {:title "Very Serious Title.md"})
  (add-output-file-name {:title "a-normal-title.md"}))

(defn generate-output-file-names
  []
  (let [pages (:pages @site)]
    (swap! site assoc :pages (map add-output-file-name pages))))
