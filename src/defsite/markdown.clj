(ns defsite.markdown
  (:require
    [clojure.java.io :as io]
    [clojure.spec.alpha :as s]
    [clojure.string :as str]
    [markdown.core :as md]
    [hickory.core :as hickory]
    [me.raynes.fs :as fs]
    [defsite.paths :as paths]
    [defsite.theme :as theme])
  (:import (org.yaml.snakeyaml.scanner ScannerException)
           (java.io StringReader StringWriter)))

(defn- take-md-files-and-create-full-path
  [[root _ files]]
  (let [root (str root)]
    (->> (filter #(str/ends-with? (str %) ".md") files)
         (map #(str root "/" %)))))

(defn find-all-markdown-files
  "Searches recursively for all '*.md' files in the given directory, and returns a list of their full path."
  [content-dir]
  (->> (fs/iterate-dir content-dir)
       (map take-md-files-and-create-full-path)
       (filter #(not (empty? %)))
       (into [] cat)))

(comment
  (require '[clojure.java.io :as io])
  (find-all-markdown-files (io/file ".")))

(defn read-markdown-and-metadata
  [md-file-path]
  (md/md-to-html-string-with-meta (slurp md-file-path)))

(comment
  (def metadata (:metadata (read-markdown-and-metadata "/home/dominik/Projects/defsite/./hyde/src/content/standalone-article.md")))
  (update-keys (into (hash-map) metadata) (fn [k] (keyword (str/replace (str/lower-case (name k)) " " "-"))))
  (keyword (str/replace (str/lower-case (name "Hello There")) " " "-")))

;; This is currently only used *directly* at understandingtheeu - here in defsite it's not used
;; TODO: However, we should use the transformers (see below) again! Either via this function, or in another
(defn get-md-content
  [md-path]
  (-> (markdown.core/md-to-html-string
        (slurp (io/resource md-path))
        :custom-transformers [theme/add-tailscale-classes-for-md-content])))

(defn md-to-hiccup
  [md-content]
  (-> md-content
      md/md-to-html-string
      hickory/parse
      hickory/as-hiccup))

(defn transform-metadata
  [metadata]
  (update-keys (into (hash-map) metadata) (fn [k] (keyword (str/replace (str/lower-case (name k)) " " "-")))))

(defn- md-to-html-string-with-meta
  [md-content]
  (try
    (md/md-to-html-string-with-meta md-content)
    (catch ScannerException e
      (throw
        (ex-info
          (str
            "The Markdown scanner encountered an exception. One common problem are multiline frontmatter values or "
            "colons in the values; in this case, check that you wrapped them with quotes.\n\n"
            "The original exception message: \n\n"
            (.getMessage e))
          {:scanner-exception e})))))

(defn- remove-md-file-from-path
  "If the path contains the actual file (in our case it normally will), remove it.

  If the given path doesn't end in '.md', the same path is returned."
  [md-file-path]
  (if (str/ends-with? md-file-path ".md")
    (paths/remove-file-from-paths md-file-path)
    md-file-path))

(defn remove-initial-slash-if-any
  [path]
  (if (and (not (empty? path)) (str/starts-with? path "/"))
    (subs path 1)
    path))

(defn add-last-slash-if-not-there-already
  [path]
  (if (or (empty? path) (str/ends-with? path "/"))
    path
    (str path "/")))

(defn- get-relative-path-for-md-file
  [md-full-file-path content-dir]
  (-> md-full-file-path
      (str/replace content-dir "")
      (remove-md-file-from-path)
      (remove-initial-slash-if-any)
      (add-last-slash-if-not-there-already)))

(comment
  (get-relative-path-for-md-file
    "/home/dominik/Projects/understandingtheeu/src/content/articles/ideas.md"
    "/home/dominik/Projects/understandingtheeu/src/content/"))

(defn md-content-to-page
  [md-file-path]
  (let [relative-path (get-relative-path-for-md-file md-file-path (paths/get-content-dir))
        md-content (slurp md-file-path)

        html-with-metadata (md-to-html-string-with-meta md-content)
        html-as-hiccup (-> (:html html-with-metadata) hickory/parse hickory/as-hiccup)
        ;; TODO: Maybe rename namespace-path to relative-path
        metadata (assoc (transform-metadata (:metadata html-with-metadata)) :namespace-path relative-path)
        unconformed-page (assoc metadata :hiccup html-as-hiccup)
        page (s/conform :defpage/page unconformed-page)]
    (if (s/invalid? page)
      (throw (ex-info
               (str "Markdown page doesn't conform to the page type.\n\n"
                    (s/explain-data :defpage/page unconformed-page))
               {:page       unconformed-page
                :md-content md-content}))
      page)))

(comment
  ;; Below, the first one throws a nice exception, because "Description" is multi-line, but not in quotes.
  ;; Via the exception message we should be able to understand the error quite quickly.
  (md-to-html-string-with-meta
    "---\nDescription: Borders as a concept are quite fascinating in itself. I think almost everyone in the EU knows by\nnow how complicated outer borders are, with the refugee crises and all that. But not only that I find interesting,\nbut also how the inner borders of the EU are still very much relevant today. The thing I have in mind here\nare the people that don't fully feel that they belong to their country as much as other regions do, like\nfor example Catalonia, Scotland, or South Tyrol (where I am from).\nuid: borders-in-and-around-the-eu\n---\n\nSome text...")
  (md-to-html-string-with-meta
    "---\nDescription: \"Borders as a concept are quite fascinating in itself. I think almost everyone in the EU knows by\nnow how\ncomplicated outer borders are, with the refugee crises and all that. But not only that I find interesting, \nbut also how the inner borders of the EU are still very much relevant today. The thing I have in mind here\nare the people that don't fully feel that they belong to their country as much as other regions do, like\nfor example Catalonia, Scotland, or South Tyrol (where I am from).\"\nuid: borders-in-and-around-the-eu\n---\n\nSome text..."))
