(ns defsite.site
  (:require
    [clojure.spec.alpha :as s]))

;; TODO: Give each page a unique ID, then (maybe) make the following a map instead of a list
(s/def ::pages (s/coll-of #(s/valid? :defpage/page %)))

(s/def ::site
  (s/keys :req-un
          [::title
           ::paths
           ::pages
           ::templates]))

(def site
  "Globally stores the state of the site. HTML is stored in Hiccup, dynamic HTML that still needs to be generated
  (i.e. templates) is stored in functions.
  At the end, the functions are applied and all HTML is written out to the :root-output-dir directory.

  - paths:
      - static-content: relative path for static content
      - root-output-dir: root output directory, where the generated files are written to"
  (atom
    {:paths
     {:static-content  "static/"
      :root-output-dir "public/"}
     :pages     []
     :templates {}}))
