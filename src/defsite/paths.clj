(ns defsite.paths
  (:require
    [clojure.java.io :as io]
    [clojure.string :as str]
    [clojure.java.classpath]
    [me.raynes.fs :as fs]))

(defn remove-file-from-paths
  "Removes the file from the given path. Assumes that a valid path, i.e. a path including a '/', is given.
  If no '/' is found in the path, an empty string is returned."
  [path]
  (if (.contains path "/")
    (let [last-separator-index (str/last-index-of path "/")]
      (subs path 0 last-separator-index))
    ""))

(defn load-content-file
  "Load a file inside the content namespace. Expects as argument the relative path of the file, starting from
  below the content namespace/directory."
  [relative-path]
  (slurp (io/resource (str "content/" relative-path))))

(defn get-content-dir
  "Returns a string of the full path of the content directory."
  []
  (str (io/file (io/resource "content/")))
  ;; A note on the code below:
  ;; This code is another way to get the content dir. However, I found that the above way (i.e. via io/resource) is
  ;; a more reliable way.
  ;; Specifically, with the method below, it would sometimes not give me the full path, but instead just the relative
  ;; path. (Very specifically, when executed via the IntelliJ Run Configuration, it returned the full path; however,
  ;; when executed on the command line via `clj -M -m ...`, it returned the relative path. We always want the full path)
  #_(str (first (->> (clojure.java.classpath/classpath-directories)
                   (filter (fn [x] (some #(= % "content") (map fs/name (fs/list-dir x)))))))
       "/content/"))
