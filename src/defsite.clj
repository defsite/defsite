(ns defsite
  (:require
    [clojure.tools.namespace.find]
    [clojure.string :as str]
    [clojure.java.io :as io]
    [hiccup.core :as hiccup]
    [me.raynes.fs :as fs]
    [defpage]
    [defsite.theme :as theme]
    [defsite.paths :as paths]
    [defsite.markdown :as md]
    [defsite.pages :as pages]
    [defsite.site :refer [site]]))

(defn require-content-namespaces
  "We need to require each clj file inside the content namespace (package), s.t. their defpage function are also
  called. Once the namespace is require'd, its defpage will automatically be called, since it's already a function
  invocation."
  []
  (let [content-namespaces (->> (clojure.tools.namespace.find/find-ns-decls-in-dir (io/file "."))
                                (map second)
                                (filter #(str/starts-with? % "content")))]
    (println "Found" (count content-namespaces) "content namespace(s)")
    (run! require content-namespaces)))

;; TODO: Right now it adds *all* MD files - that is not what I currently want, I think! I might need to think about this
;;  more
(defn register-content-md-files
  ""
  []
  (let [content-dir (paths/get-content-dir)
        all-md-file-paths (md/find-all-markdown-files content-dir)
        all-md-pages (map md/md-content-to-page all-md-file-paths)]
    (doseq [md-page all-md-pages]
      (swap! site update :pages conj md-page))))

(defn require-template-namespaces
  "We need to require each clj file inside the content namespace (package), s.t. their defpage function are also
  called. Once the namespace is require'd, its defpage will automatically be called, since it's already a function
  invocation."
  []
  (let [template-namespaces (->> (clojure.tools.namespace.find/find-ns-decls-in-dir (io/file "."))
                                 (map second)
                                 (filter #(str/starts-with? % "templates")))]
    (println "Found" (count template-namespaces) "template namespace(s)")
    (run! require template-namespaces)))

(defn get-index-page-html
  []
  (let [index-page-template-fn (:index (:templates @site))]
    (hiccup/html (index-page-template-fn @site))))

(defn process-pages
  "Currently, pages come either from MD files directly, or from a (defpage ...) in ClJ source code.

  While they have mostly the same tags, we need to make them ready for generation, which is done here."
  []
  (pages/generate-output-file-names))

(defn defsite
  [defsite]
  (swap! site merge defsite)

  (require-content-namespaces)
  (register-content-md-files)
  (require-template-namespaces)

  (process-pages)

  (swap! site assoc-in [:paths :root-output-dir] (or (:root-output-dir defsite) (-> @site :paths :root-output-dir)))

  (println "Generating site in" (-> @site :paths :root-output-dir))
  (fs/mkdir (-> @site :paths :root-output-dir))

  (println "Generating index page")
  (spit (str (-> @site :paths :root-output-dir) "index.html") (get-index-page-html))

  (println "Generating" (count (:pages @site)) "page(s)")
  (doseq [page (:pages @site)]
    (let [page-full-path (str (-> @site :paths :root-output-dir) (:namespace-path page) (:output-filename page) ".html")
          page (assoc page :namespace-path (:namespace-path page))]
      (println "Creating page:" page-full-path)
      (io/make-parents page-full-path)
      (spit page-full-path (defpage/get-page-html page))))

  (println "Copying static content over to output directory")
  (io/make-parents (str (-> @site :paths :root-output-dir) (-> @site :paths :static-content)))
  (fs/delete-dir (str (-> @site :paths :root-output-dir) (-> @site :paths :static-content)))
  (fs/copy-dir
    (-> @site :paths :static-content io/resource io/file)
    (io/file (str (-> @site :paths :root-output-dir) (-> @site :paths :static-content)))))
