(ns deftemplate
  (:require
    [clojure.spec.alpha :as s]
    [defsite.site :refer [site]]))

(s/def ::deftemplate
  (s/keys :req-un [::name ::template-fn]))

(s/def ::deftemplate-index
  (s/keys :req-un [::template-fn]))

(s/def ::deftemplate-page
  (s/keys :req-un [::template-fn]))

(defn deftemplate
  "Define a template.

  * :name - the name of the template; normally a keyword
  * :template-fn - the template function; as parameter it gets the content"
  [deftemplate]
  {:pre  [(s/valid? ::deftemplate deftemplate)]
   :post [(s/valid? :defsite/site @site)]}
  ;; TODO: Make a function conform!, that fails with a good error message if not all keys are present; then replace all
  ;;  s/conform here with my/conform!
  (let [{:keys [name template-fn]} (s/conform ::deftemplate deftemplate)]
    (println "Registering template:" name)
    (swap! site update :templates assoc name template-fn)))

(defn deftemplate-index
  "Like deftemplate, but defines an index template, of which there must exist exactly one.
  The index template will be used for the homepage of the site.

  Compared to deftemplate this must not have a :name keyword, as that will always be hardcoded to :index for this."
  [deftemplate]
  {:pre  [(s/valid? ::deftemplate-index deftemplate)]}
  (let [{:keys [template-fn]} (s/conform ::deftemplate-index deftemplate)]
    (println "Registering index template (:index)")
    (swap! site update :templates assoc :index template-fn)))

(defn deftemplate-page
  "Like deftemplate, but defines a page, i.e. any page inside 'content' that doesn't have its own template defined.

  Compared to deftemplate this must not have a :name keyword, as that will always be hardcoded to :page for this.

  Furthermore, an important difference here is also the :template-fn function. This will allow two parameters: Next to
  the normal 'site' parameter, which contains site metadata, it will also contain a 'page' parameter. The latter
  contains page metadata as well as its full hiccup function, that can (and should) be called here."
  ;; TODO: As in the comment above, change :hiccup of a page into :hiccup-fn.
  ;;  Now, this is actually a bit more complicated, because here we also often have :md-file. Currently, these two
  ;;  have the same result, which, like so makes sense.
  ;;  For now, we should change it like that, that for :md-file, we store a hiccup-fn that whatever the 'site' argument
  ;;  simply returns the hiccup of the MD file. Thus, we always store the :hiccup-fn in the site atom.
  [deftemplate]
  {:pre  [(s/valid? ::deftemplate-page deftemplate)]
   :post [(s/valid? :defsite/site @site)]}
  (let [{:keys [template-fn]} (s/conform ::deftemplate-page deftemplate)]
    (println "Registering page template (:page)")
    (swap! site update :templates assoc :page template-fn)))
