(ns defpage
  (:require
    [clojure.spec.alpha :as s]
    [clojure.string :as str]
    [hiccup.core :as hiccup]
    [defsite.markdown :as md]
    [defsite.paths :as paths]
    [defsite.site :refer [site]]))

(s/def ::defpage
  (s/keys :req-un [::title]
          :opt-un [::hiccup ::md-file ::tags ::template]))

(defn- change-path-to-start-from-below-content-dir
  [path]
  (if (and (not (empty? path)) (str/starts-with? path "content/"))
    (subs path 8)
    path))

(defn rewrite-namespace-to-folder
  [namespace-string]
  (let [last-separator-index (str/last-index-of namespace-string ".")]
    [(-> (subs namespace-string 0 last-separator-index)
         (str/replace "." "/")
         (str "/")
         (change-path-to-start-from-below-content-dir))
     (subs namespace-string (+ 1 last-separator-index))]))

(s/def ::page
  ;; TODO: Maybe rename namespace-path to content-path
  (s/keys :req-un [::title ::namespace-path]
          :opt-un [::hiccup ::draft ::description ::uid]))

(defn defpage
  "Define a page. A page has meta attributes and its content. Its content is defined in the :hiccup tag.

  Further, we have the following tags to describe meta attributes of the page:
  * :title - a string containing the title of the page
  * :hiccup - the content of the page, as hiccup
  * :template - to define the template explicitly
  * :tags - define tags for this page"
  ;; TODO: Further tags needed: :file, for HTML or MD files,
  ;;  :url-slug, to define a specific url slug
  ;;  :draft
  ;;  :description
  ;;  :uid - or how do we call this? basically, a manually set (or automatically if not set) universal ID
  [defpage]
  {:pre  [(s/valid? ::defpage defpage)]
   :post [(s/valid? :defsite.site/site @site)]}
  (let [page (s/conform ::defpage defpage)
        [namespace-path _] (rewrite-namespace-to-folder (str *ns*))
        page (assoc page :namespace-path namespace-path)]
    (if (and (-> page :hiccup empty? not) (-> page :md-file empty? not))
      (throw (RuntimeException. (str "Only either :hiccup or :md-file, but not both, can be set for defpage.\nErroneous defpage is: " page))))
    (println "Registering page:" (:title page))
    (swap! site update :pages conj page)))

(defn get-page-html
  [page]
  ;; TODO: Should we in the normal case use a :template/page template, i.e. a standard template?
  ;;  (We'd still need to define that template though!)
  ;;  (Note also that we should then replace "identity" below)
  (let [template-fn (or (get (:templates @site) (:template page)) identity)]
    (hiccup/html (template-fn (:hiccup page)))))