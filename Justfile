
# Note that we need this to generate JARs and deploy:
# https://github.com/practicalli/clojure-deps-edn

generate-pom:
  clojure -Spom

  @printf "Generated pom.xml. Please update version accordingly!\n\n"
  @read -p "Press Enter once you have updated it..."
  @printf "Done\n"

build-jar: generate-pom
  clojure -X:project/jar :jar '"defsite.jar"' :aot false :main-class defsite.defsite

deploy-locally: build-jar
  clojure -X:deps mvn-install :jar '"defsite.jar"'
